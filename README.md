# README #

In this project, I use Selenium Java, Cucumber and Maven

1. Install Java JDK 1.8 u201 and set up Java environment 
2. Install and setup Maven follow the link: https://www.mkyong.com/maven/how-to-install-maven-in-windows/
3. Install Cucumber plugin 
----Launch Eclipse
----Go to: Help > Eclipse Marketplace
----Search for "Cucumber" > Install "Natural"
----Restart Eclipse
4. Import Maven project to Eclipse: https://www.lagomframework.com/documentation/1.5.x/java/EclipseMavenInt.html
5. Right click on project > Select Maven > Update Project to download library
6. Go to TestRuner.java in Selenium.Framework package > run local and check result

Note: If you cannot run this project, please update the newest Firefox version for development
Please check the Java environment: must be: JavaSE-1.7(jdk1.8.0_201)