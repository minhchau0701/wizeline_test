package handler;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.UnreachableBrowserException;

public class Browser {
	
	public static WebDriver driver;
	
	public void launchChromeBrowser(String webURL) {
		try {
			System.setProperty("webdriver.chrome.driver", ".//resources/chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(webURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

	public void launchFirefoxBrowser(String webURL) {
		System.setProperty("webdriver.gecko.driver", ".//resources/geckodriver.exe");
		System.out.println("Start web");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = new FirefoxDriver(capabilities);
		driver.navigate().to(webURL);
	}
	
	public void launchBrowser(String webURL, String browser) {
		
		Browser page = new Browser();
		switch (browser) {
		case "Google Chrome":
			page.launchChromeBrowser(webURL);
			break;
			
		case "Firefox":
			page.launchFirefoxBrowser(webURL);
			break;
		
		default:
			throw new UnreachableBrowserException("Browser is invalid!");
		}
	}
	
	public void closeBrowser() {
		driver.manage().deleteAllCookies();
		driver.quit();
		driver = null;
	}
	
	
}
