package Selenium.Framework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Cucumber_Hooks.PropertiesFile;
import handler.*;


public class SeleniumTest {


	public void printConsole() {
		System.out.println("----- Testing ---- ");

	}


	
	public void loginSystem (String username, String password) {
		Login login= new Login(); 
		login.loginSystem(username, password);
	
	}
	
	public void selectAddCart () {
		
		PropertiesFile.getElementbyXpath("item1").click();
	
	}
	
	public void selectCartIcon () {
		
		PropertiesFile.getElementbyXpath("cartIcon").click();
	
	}
	
	public void selectCheckout () {
		
		PropertiesFile.getElementbyXpath("checkoutBtn").click();
	
	}
	
	public void enterInformation (String firstname, String lastname, String postalcode) {
		
		PropertiesFile.getElementbyXpath("form_firstnameTxt").sendKeys(firstname);
		PropertiesFile.getElementbyXpath("form_lastnameTxt").sendKeys(lastname);
		PropertiesFile.getElementbyXpath("form_postalcodeTxt").sendKeys(postalcode);
	}
	
	public void selectContinue () {
		
		PropertiesFile.getElementbyXpath("form_continueBtn").click();
	}

	public void selectFinish () {
		
		PropertiesFile.getElementbyXpath("overview_finishBtn").click();
	}
	
	public void assertCheckout () {
		PropertiesFile.getElementbyXpath("confirmation_finishfrm");
	}



}
