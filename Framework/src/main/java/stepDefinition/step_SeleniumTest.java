package stepDefinition;

import Selenium.Framework.*;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import handler.*;

public class step_SeleniumTest {
	
	
	@Given("^Launching website \"([^\"]*)\" on the \"([^\"]*)\" browser$")
	public void launching_website_on_the_browser(String webURL, String browser) {
		Browser test = new Browser();
		test.launchBrowser(webURL, browser);
	}

	@Given("^Logging into the system with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void logging_into_the_system_with_username_and_password(String username, String password) {
		SeleniumTest test = new SeleniumTest();
		test.loginSystem(username, password);
	}


	@Given("^User selects the Add to cart button$")
	public void user_selects_the_Add_to_cart_button()  {
		SeleniumTest test = new SeleniumTest();
		test.selectAddCart();
	}

	@Given("^User goes to the Cart page$")
	public void user_goes_to_the_Cart_page()  {
		SeleniumTest test = new SeleniumTest();
		test.selectCartIcon();
	}

	@Then("^User selects the Checkout button$")
	public void user_selects_the_Checkout_button()  {
		SeleniumTest test = new SeleniumTest();
		test.selectCheckout();
	    
	}

	@Then("^User enters the information about Firstname \"([^\"]*)\", Lastname \"([^\"]*)\" and Postal code \"([^\"]*)\"$")
	public void user_enters_the_information_about_Firstname_Lastname_and_Postal_code(String firstname, String lastname, String postalcode) {
		SeleniumTest test = new SeleniumTest();
		test.enterInformation(firstname, lastname, postalcode);
	}

	@Then("^User selects the Continue button$")
	public void user_selects_the_Continue_button() {
		SeleniumTest test = new SeleniumTest();
		test.selectContinue();
	}

	@Then("^User selects the Finish button$")
	public void user_selects_the_Finish_button()  {
		SeleniumTest test = new SeleniumTest();
		test.selectFinish();
	}

	@Then("^The successful page is displayed$")
	public void the_successful_page_is_displayed() {
		SeleniumTest test = new SeleniumTest();
		test.assertCheckout();
	}

}
