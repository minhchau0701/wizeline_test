package Cucumber_Hooks;

import Selenium.Framework.*;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import handler.Browser;

public class Hooks {
	
	@After 
	public void closeBrowse () {
		Browser test = new Browser();
		test.closeBrowser();
	}
	
	@Before
	public void readFileListFromFolder () {
		PropertiesFile.readFileListFromFolder();
	}
	
	
}
