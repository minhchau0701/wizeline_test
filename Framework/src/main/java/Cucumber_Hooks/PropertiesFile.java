package Cucumber_Hooks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import handler.Browser;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PropertiesFile {
	
	public static Properties properties = new Properties();
	public static FileInputStream objfile;

	public static void readPropertiesFile (String filename) {
		
		Properties obj =  new Properties();
		try {
			obj = new Properties();
			objfile = new FileInputStream(filename);
			obj.load(objfile);
			properties.putAll(obj);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}
	
	public static void readFileListFromFolder () {
		File file = new File("src/main/java/config/");
		File[] files = file.listFiles();
		for (File f: files) {
			readPropertiesFile(f.getPath());
		}
 	}
	public static WebElement getElementbyXpath (String xPath) {
		
		WebElement element = (new WebDriverWait(Browser.driver, 5))
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty(xPath))));
	
		return element;
	}

}
