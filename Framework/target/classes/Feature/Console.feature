Feature: Print Console

Scenario: Printing the text on console
Given Launching website "https://www.saucedemo.com/index.html" on the "Firefox" browser 
And Logging into the system with username "standard_user" and password "secret_sauce"
And User selects the Add to cart button
And User goes to the Cart page
Then User selects the Checkout button
And User enters the information about Firstname "Chau", Lastname "Ngo" and Postal code "700000"
And User selects the Continue button
Then User selects the Finish button
Then The successful page is displayed